<?php

use Illuminate\Database\Seeder;

class StudentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\School::all()->each(function (\App\School $school) {
            factory(\App\Student::class, rand(10, 30))->create([
                'school_id' => $school->id
            ]);
        });
    }
}
