<?php

use Illuminate\Database\Seeder;

class TeacherSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\School::all()->each(function (\App\School $school) {
            factory(\App\Teacher::class, rand(5, 10))->create([
                'school_id' => $school->id
            ]);
        });
    }
}
