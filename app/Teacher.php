<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    protected $fillable = ['name', 'last_name'];

    public function school()
    {
        return $this->belongsTo(School::class);
    }
}
