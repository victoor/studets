<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\StudentStore;
use App\Http\Resources\StudentResource;
use App\School;
use App\Student;

class StudentsController extends Controller
{
    public function index()
    {
        return StudentResource::collection(Student::paginate());
    }

    public function store(StudentStore $student)
    {
        $school = School::find($student->get('school_id'));
        $student = $school->students()->create($student->except('school_id'));

        return new StudentResource($student);
    }
}