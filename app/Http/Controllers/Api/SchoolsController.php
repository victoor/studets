<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\SchoolResource;
use App\School;
use Illuminate\Http\Request;

class SchoolsController extends Controller
{
    public function index()
    {
        return SchoolResource::collection(School::paginate());
    }
}
