<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\TeacherStore;
use App\Http\Resources\StudentResource;
use App\Http\Resources\TeacherResource;
use App\School;
use App\Teacher;

class TeachersController extends Controller
{
    public function index()
    {
        return TeacherResource::collection(Teacher::paginate());
    }

    public function store(TeacherStore $teacher)
    {
        $school = School::find($teacher->get('school_id'));
        $teacher = $school->teachers()->create($teacher->except('school_id'));

        return new StudentResource($teacher);
    }
}