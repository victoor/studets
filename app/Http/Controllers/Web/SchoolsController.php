<?php

namespace App\Http\Controllers\Web;

use App\School;
use Illuminate\Routing\Controller;

class SchoolsController extends Controller
{
    public function index()
    {
        return view('schools.index', [
            'schools' => School::paginate(),
        ]);
    }

    public function view(School $school)
    {
        return view('schools.school', [
            'school' => $school,
        ]);
    }
}