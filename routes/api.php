<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('schools', 'Api\SchoolsController@index');

Route::get('students', 'Api\StudentsController@index');
Route::post('students', 'Api\StudentsController@store');

Route::get('teachers', 'Api\TeachersController@index');
Route::post('teachers', 'Api\TeachersController@store');
