@extends('layouts.main')

@section('content')
    <div class="row">
        <div class="col">
            <table class="table">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Location</th>
                </tr>
                </thead>

                <tbody>
                @foreach($schools as $school)
                    <tr>
                        <td><a href="{{ route('schoolView', $school) }}">{{ $school->name }}</a></td>
                        <td>{{ $school->location }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <div class="row">
        <div class="col-6">
            @if (!$schools->onFirstPage())
                <a href="{{ $schools->previousPageUrl() }}">{{ __('« Previous page') }}</a>
            @endif
        </div>

        <div class="col-6">
            @if ($schools->hasMorePages())
                <a href="{{ $schools->nextPageUrl() }}">{{ __('Next page »') }}</a>
            @endif
        </div>
    </div>
@endsection