@extends('layouts.main')

@section('content')
    <h2>{{ $school->name }}</h2>
    <p>{{ $school->location }}</p>

    <h3>Teachers</h3>
    <table class="table">
        <thead>
        <tr>
            <th>Name</th>
            <th>Last name</th>
        </tr>
        </thead>

        <tbody>
        @foreach($school->teachers as $teacher)
            <tr>
                <td>{{ $teacher->name }}</td>
                <td>{{ $teacher->last_name }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <h3>Students</h3>
    <table class="table">
        <thead>
        <tr>
            <th>Name</th>
            <th>Last name</th>
        </tr>
        </thead>

        <tbody>
        @foreach($school->students as $student)
            <tr>
                <td>{{ $student->name }}</td>
                <td>{{ $student->last_name }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection